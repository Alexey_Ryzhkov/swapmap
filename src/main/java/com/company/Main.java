package com.company;

import java.util.*;

public class Main {
    public static Map<Collection<String>,Integer> Swap(Map<Integer, String> map) {
        Collection<Integer> allKeys = map.keySet();//Получаем коллекцию ключей
        List<String> allValues = new ArrayList<String>();
        Map<Collection<String>, Integer> newMap = new HashMap<Collection<String>, Integer>();
        map.forEach((key, value) -> {//Получаем все значения в Map`е
            allValues.add(value);
        });
        while (allValues.size() > 0) {
            List<String> newKey = new ArrayList<String>();//Список который будет в дальнейшем выступать как новый ключ
            String tmpStr = allValues.get(0);
            allValues.forEach(value -> {//Формируем новый ключ
                if (tmpStr.equals(value)) newKey.add(value);
            });
            allValues.removeIf(tmpStr::equals);//Удаляем все значения, которые уже использовали для формирования нового ключа
            allKeys.forEach(key->{//Формируем новый Map используя как ключевое поле список, а в качестве значения ключ
                if (map.get(key).equals(newKey.get(0))){
                    newMap.put(newKey,key);
                }
            });
        }
        return newMap;
    }

    public static void main(String[] args) {
        Map<Integer, String> map = new HashMap<Integer, String>();
        map.put(1, "One");
        map.put(2, "Two");
        map.put(3, "Two");
        map.put(4, "One");
        map.put(10, "Three");
        map.put(6,"One");
        Map<Collection<String>,Integer> newMap = Swap(map);
        System.out.println("Изначальный Map \n" + map.toString());
        System.out.println("Map после преобразования \n" + newMap.toString());
    }
}
